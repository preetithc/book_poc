from django.shortcuts import render
from django.http import HttpResponse


# def index(request):
#     return HttpResponse('<h1>Django.</h1>')

def index(request):
    person = {'firstname': 'Craig', 'lastname': 'Daniels'}
    weather = "sunny"
    context = {
        'person': person,
        'weather': weather,
    }
    return render(request,'index.html',context)

