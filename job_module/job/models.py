from django.db import models
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator

# Create your models here.

class Job(models.Model):
    program_id = models.CharField(max_length=50)
    job_title = models.CharField(max_length=255,verbose_name="Title",unique=True)
    job_type = models.CharField(max_length=100,)
    category = models.CharField(max_length=100)
    hire_type = models.CharField(max_length=100)
    company_name = models.CharField(max_length=200)
    job_label = models.CharField(max_length=100,)
    no_of_openings = models.IntegerField()
    location = models.CharField(max_length=100,)
    salary_min_range = models.IntegerField()
    salary_max_range = models.IntegerField()
    job_description = models.TextField()
    job_start_date =models.DateTimeField()
    job_end_date = models.DateTimeField()
    approve = models.BooleanField(default=True)
    status = models.BooleanField(default=True)
    column_1 = models.CharField(max_length=255,null=True,blank=True)
    column_2 = models.CharField(max_length=255,null=True,blank=True)
    column_3 = models.CharField(max_length=255,null=True,blank=True)
    column_4 = models.CharField(max_length=255,null=True,blank=True)
    column_5 = models.CharField(max_length=255,null=True,blank=True)
    column_6 = models.CharField(max_length=255,null=True,blank=True)
    column_7 = models.CharField(max_length=255,null=True,blank=True)
    column_8 = models.CharField(max_length=255,null=True,blank=True)
    column_9 = models.CharField(max_length=255,null=True,blank=True)
    column_10 = models.CharField(max_length=255,null=True,blank=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE,related_name='created_by_user')
    modified_by = models.ForeignKey(User,on_delete=models.CASCADE,related_name='modified_by_user')
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.job_title

# # file model
# class File(models.Model):
#     file = models.ForeignKey(Job, related_name='file', on_delete=models.CASCADE)
#     file_upload = models.FileField(upload_to="jobs",validators=[FileExtensionValidator(allowed_extensions=['json'])])
#
#     # def __str__(self):
#     #     return self.file
class ConfiguratorFile(models.Model):
    """
    defining config file to allow a validation for job attributes
    """
    job = models.ForeignKey(Job,on_delete=models.CASCADE,related_name='job_configurator')
    program_id = models.CharField(max_length=50)
    configurator_file = models.FileField(upload_to='static/configurator/',null=True,blank=True)

    class Meta:
        verbose_name='ConfiguratorFile'
        verbose_name_plural = 'ConfiguratorsFile'
