from django.urls import path
from job import views
app_name = 'job'


urlpatterns = [
    path('',views.JobView.as_view(),name='joblist'),
    path('edit-job/<int:pk>/',views.JobDetailView.as_view(),name='detailjob'),
    path('upload/',views.FileUploadView.as_view(), name='file-upload')

]
