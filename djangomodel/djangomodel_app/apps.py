from django.apps import AppConfig


class DjangomodelAppConfig(AppConfig):
    name = 'djangomodel_app'
