from django.urls import path
from djangomodel_app import views

urlpatterns = [
    path('', views.users,name='users'),
]