from django.shortcuts import render
from djangomodel_app.models import User

# create view here


def index(request):
    return render(request,'index.html')

def users(request):
    user_list = User.objects.order_by('first_name')
    user_dict = {'users':user_list}
    return render(request,'user.html',context=user_dict)
