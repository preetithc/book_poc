# Generated by Django 3.0.8 on 2020-08-25 05:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('job', '0003_auto_20200824_2332'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConfiguratorFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('program_id', models.CharField(max_length=50)),
                ('configurator_file', models.FileField(blank=True, null=True, upload_to='static/configurator/')),
                ('job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='job_configurator', to='job.Job')),
            ],
            options={
                'verbose_name': 'ConfiguratorFile',
                'verbose_name_plural': 'ConfiguratorsFile',
            },
        ),
        migrations.DeleteModel(
            name='File',
        ),
    ]
