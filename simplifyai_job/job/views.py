from django.shortcuts import render,HttpResponse,get_object_or_404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.pagination import LimitOffsetPagination
from job.serializers import JobSerializer,FileSerializer
from job.models import Job
from .models import ConfiguratorFile
from rest_framework.parsers import FileUploadParser




# Create your views here
class JobView(APIView):
    """
    job listing and create
    """
    permission_classes = [AllowAny]
    pagination_class = LimitOffsetPagination

    def get(self,request):
        job_obj = Job.objects.filter(status=True)
        paginator = LimitOffsetPagination()
        results = paginator.paginate_queryset(job_obj, request)
        serializer = JobSerializer(results,many=True)
        return Response(serializer.data)

    def post(self,request):
        job = request.data
        serializer = JobSerializer(data=job)
        try:
            if serializer.is_valid(raise_exception=True):
                job_saved = serializer.save()
                return Response({"success":"Job {} saved successfully".format(job_saved.job_title),'status':status.HTTP_201_OK})
        except:
            return Response({"error":"job is not save",'status':status.HTTP_400_BAD_REQUEST})

class JobDetailView(APIView):
    """
    single job list, update job, delete job used pk
    """
    def get(self,request,pk):
        job_obj = Job.objects.filter(id=pk,status=True)
        serializer = JobSerializer(job_obj,many=True)
        return Response(serializer.data)

    def put(self,request,pk):
        saved_job = get_object_or_404(Job.objects.all(), pk=pk,status=True)
        data = request.data
        serializer = JobSerializer(instance=saved_job, data=data, partial=True)
        try:
            if serializer.is_valid(raise_exception=True):
                job_save = serializer.save()
                return Response({"success": "Job '{}' updated successfully".format(job_save.job_title),'status':status.HTTP_200_OK})
        except:
            return Response({"error":"Job is not update",'status':status.status.HTTP_400_BAD_REQUEST})

    def delete(self,request,pk):
        saved_job = Job.objects.filter(id=pk).update(status=False)
        if saved_job:
            return Response({"success": "Job id '{}' delete successfully".format(pk)},status=204)
        else:
            return Response({"success": "Job id '{}' is not delete ".format(pk)},status=404)

class FileUploadView(APIView):
    parser_class = (FileUploadParser,)
    def post(self, request):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)