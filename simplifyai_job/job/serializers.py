from rest_framework import serializers
from job.models import Job
from .models import ConfiguratorFile

class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model= Job
        fields ='__all__'

class FileSerializer(serializers.ModelSerializer):
  class Meta():
    model = ConfiguratorFile
    # fields = ('file','file_upload')
    fields = '__all__'